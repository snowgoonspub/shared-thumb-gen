#
# Shared Thumbnail Generator
# Tim Walls <tim.walls@snowgoons.com> - https://snowgoons.com/
#
import hashlib
import os
import posixpath
import urllib.parse
from dataclasses import dataclass
from enum import Enum
from pathlib import Path
from PIL import Image

THUMBNAIL_SUBDIR = ".sh_thumbnails"
THUMBNAIL_SUFFIX = ".png"
THUMBNAIL_FORMAT = "png"


@dataclass
class ThumbnailSpec:
  """
  Represents the specifications for generating thumbnails.

  :ivar width: The width of the thumbnail in pixels.
  :vartype width: int
  :ivar height: The height of the thumbnail in pixels.
  :vartype height: int
  :ivar subdir: The subdirectory where the generated thumbnails will be stored.
  :vartype subdir: str
  """
  width: int
  height: int
  subdir: str
  hash_prefix: str

  def thumbpath_for_file(self, filename):
    """
    :param filename: The filename of the file for which to generate thumbnails of this spec
    :return: The path to the thumbnail file.
    """
    return Path(os.path.join(filename.file_dir, THUMBNAIL_SUBDIR, self.subdir, filename.thumb_name(self)))

  def size(self):
    """
    Returns the width and height of the object.

    :return: A tuple containing the width and height.
    :rtype: tuple
    """
    return self.width, self.height

  def is_xfce4_compatible(self):
    """
    Check if the object is compatible with Xfce4.

    :return: True if the hash_prefix is an empty string, False otherwise.
    :rtype: bool
    """
    return self.hash_prefix == ""


@dataclass
class GeneratedThumbnail:
  src_mtime: float


@dataclass
class ThumbFileCreator:
  """
  Represents a file name with its directory, path, and thumbnail name.

  :param file_dir: The directory of the file.
  :type file_dir: str
  :param file_name: The name of the file.
  :type file_name: str
  :param file_path: The path of the file.
  :type file_path: posixpath
  """
  file_dir: str
  file_name: str
  file_path: Path

  @classmethod
  def from_filename(cls, filename):
    """
    Creates a ThumbFilename object from a given filename.

    :param filename: The full path of the file.
    :type filename: str
    :return: The created Filename object.
    :rtype: ThumbFileCreator
    """
    (dir, file) = os.path.split(os.path.abspath(filename))

    return cls(
      file_dir=dir,
      file_name=file,
      file_path=Path(os.path.join(dir, file))
    )

  def thumb_name(self, spec: ThumbnailSpec):
    """
    Return the thumbnail name for a given thumbnail specification.

    :param spec: The ThumbnailSpec object specifying the desired thumbnail.
    :type spec: ThumbnailSpec
    :return: The name of the thumbnail.
    :rtype: str
    """
    hashname = hashlib.md5(f"{spec.hash_prefix}{urllib.parse.quote(self.file_name, safe='')}".encode("utf-8")).hexdigest()

    return hashname + THUMBNAIL_SUFFIX

  def can_create_thumbnail(self):
    """
    Check if a thumbnail can be created from the given file path.

    :return: True if a thumbnail can be created, False otherwise.
    """
    # Don't try and create thumbnails for thumbnails!
    if THUMBNAIL_SUBDIR in str(self.file_path):
      return False

    # Otherwise, we want to check if it's a file and we can read it as an
    # image
    if self.file_path.is_file():
      try:
        image = Image.open(self.file_path)
        return True
      except:
        return False
    else:
      return False

  def valid_thumbnail_exists(self, spec: ThumbnailSpec):
    """
    Check if a valid thumbnail exists for the given ThumbnailSpec.

    :param spec: The ThumbnailSpec object representing the desired thumbnail specification.
    :return: True if a valid thumbnail exists, False otherwise.
    """
    my_mtime = self.get_file_mtime()

    thumbpath = spec.thumbpath_for_file(self)
    if thumbpath.is_file():
      thumb_mtime = thumbpath.stat().st_mtime

      return thumb_mtime >= my_mtime
    else:
      return False

  def get_file_mtime(self):
    """
    Returns the last modification time of the underlying source file.

    Returns:
        float: A float representing the last modification time of the file in seconds since the epoch.

    """
    return self.file_path.stat().st_mtime

  def create_thumbnail(self, spec: ThumbnailSpec):
    """
    This method creates a thumbnail for a given file using the provided ThumbnailSpec.

    :param spec: The ThumbnailSpec object that contains information about the thumbnail to be created.
    :type spec: ThumbnailSpec
    :return: The path to the generated thumbnail.
    :rtype: str
    """
    thumbpath = spec.thumbpath_for_file(self)

    # Create the directory the thumbnail will go in
    thumbpath.parent.mkdir(parents=True, exist_ok=True)

    # We want the ownership of the thumbnail directory to match the ownership
    # of our own directory (NOT necessarily the same as that of the FILE)
    sourcedir_stat = self.file_path.parent.stat()
    os.chown(thumbpath.parent.parent, sourcedir_stat.st_uid, sourcedir_stat.st_gid)
    os.chown(thumbpath.parent, sourcedir_stat.st_uid, sourcedir_stat.st_gid)

    # We will match the generated thumbnail's ownership (and permissions) to
    # that of the source FILE (to avoid unintentional information disclosure
    # where a thumbnail may be visible to a user who couldn't access the
    # file itself.)
    sourcefile_stat = self.file_path.stat()

    # OK, with that settled, let's actually generate the thumbnail
    image = Image.open(self.file_path)
    image.thumbnail(spec.size())
    image.save(thumbpath, THUMBNAIL_FORMAT)

    # Now finally correct the owneship and permissions
    os.chown(thumbpath, sourcefile_stat.st_uid, sourcefile_stat.st_gid)
    os.chmod(thumbpath, sourcefile_stat.st_mode)

    return GeneratedThumbnail(sourcefile_stat.st_mtime)


class ThumbnailSizes(Enum):
  """
  An enumeration class representing different thumbnail sizes.

  .. note::

      This class provides a simple way to define and access thumbnail specifications.

  Attributes:
      MEDIUM: A medium-sized thumbnail specification.
      LARGE: A large-sized thumbnail specification.

  """
  NORMAL = ThumbnailSpec(128, 128, "normal", "./")
  LARGE = ThumbnailSpec(256, 256, "large", "./")
  XLARGE = ThumbnailSpec(512, 512, "x-large", "./")
  XXLARGE = ThumbnailSpec(1024, 1024, "xx-large", "./")
  XFCE4_NORMAL = ThumbnailSpec(128, 128, "normal", "")
  XFCE4_LARGE = ThumbnailSpec(256, 256, "large", "")
  XFCE4_XLARGE = ThumbnailSpec(512, 512, "x-large", "")
  XFCE4_XXLARGE = ThumbnailSpec(1024, 1024, "xx-large", "")
