# Most dependencies are pretty obvious, but you will need the `docker pushrm`
# Docker extension: https://github.com/christian-korneck/docker-pushrm
#
GIT_HASH ?= $(shell git log --format="%h" -n 1)
VERSION ?= $(shell awk '/^# Release/{print $$3; exit;}' CHANGELOG.md)
DOCKER_IMAGE = snowgoons/manicured
SOURCES = $(wildcard *.py)

venv: venv/touchfile ## Install the Python virtual environment and all dependencies

build: dist/manicured ## Build the `manicured` binary

clean: ## Clean all build artifacts
	rm -rf venv
	rm -rf build
	rm -rf dist
	find -iname "*.pyc" -delete

dist/manicured: venv manicured $(SOURCES)
	. venv/bin/activate; pyinstaller --onefile manicured

venv/touchfile: requirements.txt
	test -d venv || python3 -m venv venv
	. venv/bin/activate; pip install -Ur requirements.txt
	touch venv/touchfile

dockerbuild: ## Build the docker container
	docker build --tag ${DOCKER_IMAGE}:${GIT_HASH} . -f docker/Dockerfile

dockerpush: dockerbuild ## Build docker container and push to Docker Hub
	# The next two lines ensure we stop if there are any uncommitted changes
	# in the local git
	git add . && git update-index --really-refresh
	git diff-index --quiet HEAD
	docker push ${DOCKER_IMAGE}:${GIT_HASH}
	docker pushrm ${DOCKER_IMAGE}:${GIT_HASH} -f docker/README.md

publish: build dockerpush ## Build & push docker container, and tag with latest version (given in CHANGELOG.md)
	docker pull ${DOCKER_IMAGE}:${GIT_HASH}
	docker tag ${DOCKER_IMAGE}:${GIT_HASH} ${DOCKER_IMAGE}:${VERSION}
	docker tag ${DOCKER_IMAGE}:${GIT_HASH} ${DOCKER_IMAGE}:latest
	docker push ${DOCKER_IMAGE}:${VERSION}
	docker push ${DOCKER_IMAGE}:latest

release: publish ## Build, push & tag docker container, and `git tag -a` with the latest version (given in CHANGELOG.md)
	git tag -a ${VERSION} -m "`awk '/^# Release/ && count >= 1 {exit}; /^# Release/ && ++count == 1 {p=1; next} p;' CHANGELOG.md`"
	git push origin ${VERSION}


# Now this is a neat trick: https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

_version.py: CHANGELOG.md $(filter-out $(SOURCES),_version.py)
	echo '__version__ = "manicured ${VERSION} (${GIT_HASH})"' > _version.py
