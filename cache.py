#
# Shared Thumbnail Generator
# Tim Walls <tim.walls@snowgoons.com> - https://snowgoons.com/
#
from dataclasses import dataclass
from pathlib import Path

from shared import GeneratedThumbnail, ThumbnailSizes, ThumbnailSpec, ThumbFileCreator


@dataclass
class GenerationCache:
  """

  The GenerationCache class is responsible for caching the generated thumbnails. It stores a dictionary that maps ThumbnailSpec objects to another dictionary that maps file paths to their
  * last modified time.

  Attributes:
      cache (dict): A dictionary that maps ThumbnailSpec objects to another dictionary that maps file paths to their last modified time.

  Methods:
      __init__(): Initializes the GenerationCache by creating an empty cache dictionary and initializing the inner dictionaries for each ThumbnailSpec.

      check_if_processed_thumbnail(creator: ThumbFileCreator, spec: ThumbnailSizes) -> bool: Checks if a thumbnail has been previously processed for the given ThumbFileCreator and Thumbnail
  *Sizes. Returns True if the thumbnail has been processed, False otherwise.

      record_processed_thumbnail(creator: ThumbFileCreator, spec: ThumbnailSizes): Records the processed thumbnail for the given ThumbFileCreator and ThumbnailSizes by updating the cache
  * with the file's last modified time.

  Example usage:
      cache = GenerationCache()
      creator = ThumbFileCreator(file_path='/path/to/image.jpg')
      spec = ThumbnailSizes(width=100, height=100)

      if cache.check_if_processed_thumbnail(creator, spec):
          print("Thumbnail already processed")
      else:
          # Process the thumbnail
          create_thumbnail(creator, spec)
          cache.record_processed_thumbnail(creator, spec)

  """
  cache: dict[ThumbnailSpec, dict[Path, float]]

  def __init__(self):
    """
    Initializes a new instance of the class.

    Raises:
        None

    Returns:
        None
    """
    self.cache = {}

    for spec in ThumbnailSizes:
      self.cache[spec] = {}

  def check_if_processed_thumbnail(self, creator: ThumbFileCreator, spec: ThumbnailSizes):
    """
    Args:
      creator: An instance of the ThumbFileCreator class that represents the thumbnail file creator.
      spec: An instance of the ThumbnailSizes class that specifies the thumbnail size.

    Returns:
      - True if the generated_mtime of the specified thumbnail matches or is later than the mtime of the creator's file.
      - False otherwise.
    """
    generated_mtime = self.cache[spec].get(creator.file_path, None)

    if generated_mtime:
      return generated_mtime >= creator.get_file_mtime()
    else:
      return False

  def record_processed_thumbnail(self, creator: ThumbFileCreator, spec: ThumbnailSizes):
    """
    Args:
      creator: The ThumbFileCreator object representing the thumbnail file.
      spec: The ThumbnailSizes object representing the size specifications for the thumbnail.

    """
    self.cache[spec][creator.file_path] = creator.get_file_mtime()


@dataclass
class DummyGenerationCache:
  """
  A class representing a dummy generation cache.

  Attributes:
      No attributes.

  Methods:
      check_if_processed_thumbnail: Check if a thumbnail has been processed for the given creator and spec.
      record_processed_thumbnail: Record that a thumbnail has been processed for the given creator and spec.

  """
  def __init__(self):
    pass

  def check_if_processed_thumbnail(self, creator: ThumbFileCreator, spec: ThumbnailSizes):
    """
    Checks if the thumbnail has been processed for the given ThumbFileCreator and ThumbnailSizes.
    This dummy implementation will always return False (not processed)

    Args:
      creator: The ThumbFileCreator instance that created the thumbnail.
      spec: The ThumbnailSizes instance specifying the size of the thumbnail.

    Returns:
      False
    """
    return False

  def record_processed_thumbnail(self, creator: ThumbFileCreator, spec: ThumbnailSizes):
    """
    Args:
      creator: ThumbFileCreator object, represents the creator of the thumbnail
      spec: ThumbnailSizes object, represents the specification of the thumbnail sizes

    """
    pass




