# `manicured` - a shared thumbnail generator
By default, most file managers that follow the [Thumbnail Managing Standard](https://specifications.freedesktop.org/thumbnail-spec/thumbnail-spec-latest.html)
generate thumbnails in the client and store them in a local cache on the client
(typically the `~/.thumbnails` folder).

This is great for many uses, but in certain cases - such as shared network
folders accessed over a slow link - can lead to every user of a network share
downloading the entire contents of a folder just to generate local thumbnails
whenever they are browsing a directory.  With large folders of images accessed
over something like WebDAV, this can be a performance killer.

The standard also allows for thumbnails that have been pre-generated and are
stored alongside the images themselves, in a `.sh_thumbnails` directory.
These can then be pre-generated on the file server, allowing for clients to
download only the thumbnail rather than the entire source image.

This simple script will allow generation of those thumbnails.

## Docker Container
A Docker container version of the script is available [on Docker hub](https://hub.docker.com/repository/docker/snowgoons/manicured/general)

### Attribution
Developed by Tim Walls <tim.walls@snowgoons.com>.  Released under a BSD 3-Clause
license - see [LICENSE.txt](LICENSE.txt) for details.