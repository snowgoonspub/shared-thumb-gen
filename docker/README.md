# `manicured`
[`manicured`](https://gitlab.com/snowgoonspub/shared-thumb-gen) is a simple daemon
which will monitor a folder for file creation, and automatically create
thumbnails according to the [Thumbnail Managing Standard](https://specifications.freedesktop.org/thumbnail-spec/thumbnail-spec-latest.html) "Shared Thumbnails"
spec - that is to say, the thumbnails are generated in local
`.sh_thumbnails` subdirectories alongside the files which
they represent.

This can be useful to allow pre-generation of thumbnails
server-side for folders which are shared over a network, to
avoid network clients all downloading the entire contents
of a network drive just to generate local thumbnails.

## Docker Image
This docker image simply wraps up the `manicured` executable and,
by default, runs the daemon over the directory `/var/data/thumbnails`.
To do something useful with this, you should mount a directory with some images
you want to thumbnail at that path.

### Default Command
The default command, which has sensible defaults for most uses,
is as follows:

| Command/Option                                    | Meaning                                                                                                                                                                                                                                                                        
|---------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `/bin/manicured`                                  | Command                                                                                                                                                                                                                                                                        |
| `--verbose`                                       | Output verbose logging                                                                                                                                                                                                                                                         |
| `--xfce4-bug`                                     | Create thumbnails for Xfce4 (which has a bug causing it to look for files with the wrong name) [^1]                                                                                                                                                                            |
| `--watch`                                         | Watch the directory for changes after first building thumbnails for any files aready there                                                                                                                                                                                     |
| `--poll=30`                                       | Poll for changes every 30 seconds rather than using *inotify* or a similar kernel mechanism.  This is the default because we assume that the path to thumbnail is mounted from outside the Docker runtime, and filesystem notifications will not propogate across such a mount |
| `--cache /var/data/thumbnail/.manicure-cache.stc` | Create a persistent cachefile which records metadata for which thumbnails have been generated.  This can reduce the overhead of `manicured` scanning the underlying filesystem for changes, helpful on network mounted volumes                                                 |
| `/var/data/thumbnail`                             | The path to generate thumbnails for                                                                                                                                                                                                                                            |

[^1]: Why include this by default?  Because since Xfce4's Thunar is the only
      file manager I know that actually tries to follow the share thumbnail spec
      at all, we may as well throw it a bone.