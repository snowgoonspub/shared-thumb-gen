# Release v0.2.1
* Removed some debugging output!
# Release v0.2.0
* Added -x/--exclude-regexp commandline option to allow users to exclude
  paths from processing by regexp.
# Release v0.1.1
* Fix bug with not using Canonical URI Encoded names when generating filename
  hashes.
# Release v0.1.0
* Initial version.  Supports basic scanning of directories to generate thumbnails,
  along with functionality to watch a directory for changes and generate
  thumbnails on-the-fly.
* Docker container distribution.
